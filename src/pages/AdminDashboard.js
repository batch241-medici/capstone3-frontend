import {Container, ListGroup, SplitButton, Dropdown} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Dashboard() {

	return (
		<>
		<Container>
		<div>
			<div className="h-25 w-25 d-flex">
			<img src="https://i.pinimg.com/originals/c6/21/22/c62122464463d210a1864959c9b234f7.jpg"
			className="img-fluid p-3 rounded-circle" alt="gray background" />
			<h1>Admin</h1>
			</div>

			<div className="mt-3 d-md-flex">
				<div className="me-3 col-md-3">
					<ListGroup variant="flush">
						<ListGroup.Item as={Link} to="/manageUsers">Manage Users
						</ListGroup.Item>
						<ListGroup.Item as={Link}>Manage Categories
						</ListGroup.Item>
						<ListGroup.Item>
						<SplitButton
							key='down'
							variant="secondary"
							title='Manage Products'
							>
							<Dropdown.Item as={Link} to="/product">All Products</Dropdown.Item>
							<Dropdown.Item as={Link} to="/createProduct">Create Product</Dropdown.Item>
							<Dropdown.Item>Retrieve Product</Dropdown.Item>
						</SplitButton>
						</ListGroup.Item>
						<ListGroup.Item as={Link}>Manage Orders
						</ListGroup.Item>
						<ListGroup.Item as={Link} to="/logout">Logout
						</ListGroup.Item>
					</ListGroup>
				</div>
				<div className="col-md-9 pe-3">
				</div>
			</div>
		</div>
		</Container>
		</>
	)
}