import {useState, useEffect} from 'react';

import AllUsers from '../components/AllUsers';

export default function Users() {

	const [users, setUsers] = useState([]);

	useEffect( () => {

		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUsers(data.map(user => {
				return (
					
					<AllUsers key={user._id} user={user}/>

				)
			}));
		})
	}, []);

	return (
		<>
		{users}
		</>
	)
};