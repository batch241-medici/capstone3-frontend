import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [fullName, setFullName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Please provide a different email."
				})
			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						fullName: fullName,
						email: email,
						mobileNo: mobileNo,
						password: password,
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(typeof data !== "undefined") {
						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome!"
						});

						setFullName('');
						setEmail('');
						setMobileNo('');
						setPassword('');
						setVerifyPassword('');

						navigate("/login");
					} else {
						Swal.fire({
							title: "Error on registration",
							icon: "error",
							text: "Try again later"
						})
					}
				})
			}
		})
	};

	const specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

	useEffect(() => {

		if((fullName !== '' &&  email !== '' && password !== '' && verifyPassword !== '') && (password.length > 7) && (specialCharacters.test(password) === false) && (password === verifyPassword)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [fullName, mobileNo, email, password, verifyPassword, specialCharacters]);

	return (

		(user.id !== null) ?
		<Navigate to="/" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="fullName">
				<Form.Label>Full Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your Full Name"
					value={fullName}
					onChange={e => setFullName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter your email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter your Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
				<Form.Text id="passwordHelpBlock" muted>
				Your password must be 7-14 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="verifyPassword">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify your Password"
					value={verifyPassword}
					onChange={e => setVerifyPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ (isActive) ?
			<Button variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn">
				Submit
			</Button>
			}
		</Form>
	)
};