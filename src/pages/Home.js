import Banner from '../components/Banner';
import {useEffect, useState} from 'react';

export default function Home() {

	const data = {
		title: "BuidIT",
		description: "We build your dreams!"
	}

	return (
		<>
		<Banner data={data} />
		</>
	)
}