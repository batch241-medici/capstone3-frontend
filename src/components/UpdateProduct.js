import {useState, useEffect} from 'react';
import {Container, Card, Form, Button} from 'react-bootstrap';
import {useParams, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UpdateProduct() {

	const {productId} = useParams();

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');

	function update(e)  {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data) {
					Swal.fire({
						title:"Update successful!",
						icon: "success",
						text: "Product information has been updated."
					});

					setProductName("");
					setDescription("");
					setPrice("");
					setQuantity("");
				} else {
					Swal.fire({
						title: "Update failed",
						icon: "error",
						text: "Try again later."
					})
				}
			})
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		})
	}, [productId]);

	return (
		<>
		<Container>
			<Card>
				<Card.Body>
					<Card.Title>{productName}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Subtitle>Quantity:</Card.Subtitle>
					<Card.Text>{quantity}</Card.Text>
				</Card.Body>
			</Card>

			<Form onSubmit={(e) => update(e)}>
			<Form.Group controlId="productName">
				<Form.Label>Product Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Product Name"
					value={productName}
					onChange={e => setProductName(e.target.value)}
					required />
			</Form.Group>

			<Form.Group controlId="description">
				<Form.Label>Description</Form.Label>
				<Form.Control 
					type="textarea"
					rows={3}
					placeholder="Enter product description"
					value={description}
					onChange={e => setDescription(e.target.value)}
					required />
			</Form.Group>

			<Form.Group controlId="price">
				<Form.Label>Price: Php</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter product price"
					value={price}
					onChange={e => setPrice(e.target.value)}
					required />
			</Form.Group>
			
			<Form.Group controlId="quantity">
				<Form.Label>Quantity</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter product quantity"
					value={quantity}
					onChange={e => setQuantity(e.target.value)}
					required />
			</Form.Group>
			<Button variant="success" type="submit">Update
			</Button>
			<Button variant="primary" type="submit" id="submitBtn" as={Link} to="/dashboard">
			Dashboard
			</Button>
			</Form>
		</Container>
		</>
	)
}