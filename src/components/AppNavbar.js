import {Button, Container, Form, Nav, Navbar, NavDropdown, Offcanvas} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext';

export default function AppNavbar() {

	const {user} = useContext(UserContext);

	return (
    (user.isAdmin === true) ?
		<>
      <Navbar bg="light" expand="md lg" className="mb-3">
          <Container fluid>
            <Navbar.Brand as={Link} to="/">BuildIT</Navbar.Brand>

            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-`}
              aria-labelledby={`offcanvasNavbarLabel-expand-`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-`}>
                  BuildIT
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-5">
                  <Form className="d-flex">
                    <Form.Control
                      type="search"
                      placeholder="Search"
                      className="me-2"
                      aria-label="Search"
                    />
                    <Button variant="outline-success">Search</Button>
                    <Nav.Link as={NavLink} to="/catalog">Catalog</Nav.Link>
                  </Form>
                  { (user.id !== null) ?
                    <NavDropdown
                    title="User"
                    id={`offcanvasNavbarDropdown-expand-`}
                    className="me-5"
                  >
                    <NavDropdown.Item href="#action3">Account</NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/dashboard">Dashboard</NavDropdown.Item>
                    <NavDropdown.Item href="#action4">
                      My Cart</NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action5">
                      Customer Service
                    </NavDropdown.Item>
                  </NavDropdown>
                  :
                  <>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  </>
                  }
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
    </>
    :
    <>
      <Navbar bg="light" expand="md lg" className="mb-3">
          <Container fluid>
            <Navbar.Brand as={Link} to="/">BuildIT</Navbar.Brand>

            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-`}
              aria-labelledby={`offcanvasNavbarLabel-expand-`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-`}>
                  BuildIT
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-5">
                  <Form className="d-flex">
                    <Form.Control
                      type="search"
                      placeholder="Search"
                      className="me-2"
                      aria-label="Search"
                    />
                    <Button variant="outline-success">Search</Button>
                    <Nav.Link as={NavLink} to="/catalog">Catalog</Nav.Link>
                  </Form>
                  { (user.id !== null) ?
                    <NavDropdown
                    title="User"
                    id={`offcanvasNavbarDropdown-expand-`}
                    className="me-5"
                  >
                    <NavDropdown.Item href="#action3">Account</NavDropdown.Item>
                    <NavDropdown.Item href="#action4">
                      My Cart</NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action5">
                      Customer Service
                    </NavDropdown.Item>
                  </NavDropdown>
                  :
                  <>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  </>
                  }
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
    </>

	)
}