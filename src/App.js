import './App.css';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap';

import UpdateProduct from './components/UpdateProduct';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Dashboard from './pages/AdminDashboard';
import Users from './pages/Users';
import Product from './pages/Product';
import CreateProduct from './pages/CreateProduct';
import Error from './pages/Error';
import Catalog from './pages/Catalog';

function App() {
  
	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	const unsetUser = () => {
		localStorage.clear();
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(typeof data._id !== "undefined") {
				setUser({
					id:data._id,
					isAdmin: data.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])

	return (
		<>
		<UserProvider value={{user, setUser, unsetUser}}>
		<Router>
			<AppNavbar />
			<Container>
				<Routes>
					<Route path="/" element={<Home/>}/>
					<Route path="/login" element={<Login/>}/>
					<Route path="/logout" element={<Logout/>}/>
					<Route path="/dashboard" element={<Dashboard/>}/>
					<Route path="/register" element={<Register/>}/>
					<Route path="/manageUsers" element={<Users/>}/>
					<Route path="/catalog" element={<Catalog/>}/>
					<Route path="/product" element={<Product/>}/>
					<Route path="/products/details/:productId" element={<ProductView/>}/>
					<Route path="/createProduct" element={<CreateProduct/>}/>
					<Route path="/products/:productId" element={<UpdateProduct/>}/>
					<Route path="/*" element={<Error/>}/>
				</Routes>
			</Container>
		</Router>
		</UserProvider>
		</>
	);

}

export default App;
